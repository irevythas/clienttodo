import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  uri = 'http://localhost:8080';


  constructor(private http: HttpClient) {
  }


  // Method to get all todos
  getTodos(): Observable<any> {
    return this.http.get(this.uri + '/todos');
  }

  // Method to add a new todo
  addTodo(description : string) : Observable<any> {
    
    const addUri = this.uri + '/todo';
    
    const obj = {
      description : description
    };
    
    return this.http.post(addUri, obj)
  }

  deleteTodo(id : number) : Observable<any> {
    const obj = {
      id : id
    };

    return this.http.get(this.uri + '/todo/' + id)
  }

}
