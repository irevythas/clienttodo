import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo/todo.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos: Array<any>;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.getTodos();
  }

  getTodos() {
    this.todoService.getTodos().subscribe(data => {
    this.todos = data;
    });
  }

  addTodo(description) {
    this.todoService.addTodo(description.value).subscribe(()=>{this.getTodos(); description.value="";});
  }

  deleteTodo(id: number) {
    this.todoService.deleteTodo(id).subscribe(()=>this.getTodos())
  }
}
